FROM maven:3.8.3-openjdk-17

ENV PROJECT_HOME /usr/src/demonstracao
ENV JAR_NAME app-demonstracao.jar

# Create destination directory
RUN mkdir -p $PROJECT_HOME
WORKDIR $PROJECT_HOME

# Bundle app source
COPY . .

# Package the application as a JAR file
RUN mvn clean package -DskipTests

# Move file
RUN mv $PROJECT_HOME/target/$JAR_NAME $PROJECT_HOME/

ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "app-demonstracao.jar"]



#FROM openjdk:21-jdk
#
#WORKDIR /opt
#
#ENV PORT=9010
#
#EXPOSE 9010
#
#COPY target/*.jar /opt/app.jar
#
#ENTRYPOINT exec java $JAVA_OPT -jar app.jar