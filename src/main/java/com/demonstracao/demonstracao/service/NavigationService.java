package com.demonstracao.demonstracao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demonstracao.demonstracao.domain.entity.Navigation;
import com.demonstracao.demonstracao.repository.NavigationRepository;

@Service
public class NavigationService {
	
	@Autowired
	private NavigationRepository navigationRepository;

	public List<Navigation> getAll() {
		return navigationRepository.findAll();
	}
	
}
