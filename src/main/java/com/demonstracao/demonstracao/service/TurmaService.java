package com.demonstracao.demonstracao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.demonstracao.demonstracao.domain.entity.Turma;
import com.demonstracao.demonstracao.repository.TurmaRepository;

@Service
public class TurmaService {

	@Autowired
	private TurmaRepository turmaRepository;

	public List<Turma> findAll() {
		return turmaRepository.findAll();
	}

	public Page<Turma> findAll(Pageable page) {
		return turmaRepository.findAll(page);
	}

}
