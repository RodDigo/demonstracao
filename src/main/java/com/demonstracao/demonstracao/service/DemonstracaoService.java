package com.demonstracao.demonstracao.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class DemonstracaoService {

	public List<String> nomes() {

		final List<String> nms = new ArrayList<>();

		nms.add("Rodrigo");
		nms.add("Nano");
		nms.add("Anny");

		return nms;

//		return "{\n" + "  “titulo”: “JSON x XML”,\n"
//				+ "  “resumo”: “o duelo de dois modelos de representação de informações”,\n" + "  “ano”: 2012,\n"
//				+ "  “genero”: [“aventura”, “ação”, “ficção”] \n" + " }";

	}

}
