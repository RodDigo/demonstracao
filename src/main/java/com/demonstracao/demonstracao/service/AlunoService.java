package com.demonstracao.demonstracao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.demonstracao.demonstracao.domain.entity.Aluno;
import com.demonstracao.demonstracao.domain.entity.Turma;
import com.demonstracao.demonstracao.repository.AlunoRepository;

@Service
public class AlunoService {

	@Autowired
	private AlunoRepository alunoRepository;

	public void create(Aluno aluno) {
		alunoRepository.save(aluno);
	}

	public void delete(int idAluno) {
		alunoRepository.delete(new Aluno(idAluno));
	}

	public Page<Aluno> findByNome(String nome, Pageable page) {
		return alunoRepository.findByNome(nome, page);
	}
	
	public List<Aluno> findByNome(String nome) {
		return alunoRepository.findByNome(nome);
	}

	public List<Aluno> findByTurma(int turma) {
		return alunoRepository.findByTurma(new Turma(turma));
	}

}
