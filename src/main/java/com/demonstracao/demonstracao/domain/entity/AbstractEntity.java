package com.demonstracao.demonstracao.domain.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class AbstractEntity<I extends Serializable> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected I id;
}
