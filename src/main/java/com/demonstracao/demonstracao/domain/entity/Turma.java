package com.demonstracao.demonstracao.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(schema = "db_estudo", name = "turma")
@NoArgsConstructor
@Getter
@Setter
public class Turma extends AbstractEntity<Integer> {

	private String nome;
	private String descricao;
	private String periodo;

	public Turma(int id) {
		this.id = id;
	}

}
