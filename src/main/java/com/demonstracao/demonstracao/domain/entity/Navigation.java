package com.demonstracao.demonstracao.domain.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Navigation extends AbstractEntity<Integer> {

	private String label;
	private String target;
	private String icon;
	private String iconSelected;

}
