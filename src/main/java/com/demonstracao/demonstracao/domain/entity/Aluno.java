package com.demonstracao.demonstracao.domain.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Aluno extends AbstractEntity<Integer> {

	private String nome;
	private String sobreNome;
	private String email;

	@ManyToOne
	private Turma turma;

	public Aluno(int id) {
		this.id = id;
	}

}
