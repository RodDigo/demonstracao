package com.demonstracao.demonstracao.domain.dto;

import com.demonstracao.demonstracao.domain.entity.Turma;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlunoDto {

	private String nome;
	private String sobreNome;
	private String email;
	private Turma turma;

}
