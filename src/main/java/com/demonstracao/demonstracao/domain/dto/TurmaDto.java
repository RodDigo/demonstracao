package com.demonstracao.demonstracao.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TurmaDto {

	private String nome;
	private String sobreNome;
	private String email;

}
