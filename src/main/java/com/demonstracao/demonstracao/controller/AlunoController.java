package com.demonstracao.demonstracao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.demonstracao.demonstracao.domain.dto.AlunoDto;
import com.demonstracao.demonstracao.domain.entity.Aluno;
import com.demonstracao.demonstracao.service.AlunoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/api", produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(value = "API REST YAZIGI")
@CrossOrigin(origins = "*")
public class AlunoController extends AbstractController {

	@Autowired
	private AlunoService alunoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation(value = "Cria um novo aluno.")
	public void create(@RequestBody @Valid final AlunoDto alunoDto) {
		alunoService.create(convertToEntity(alunoDto, Aluno.class));
	}

	@DeleteMapping("/alunos/delete/{idAluno}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Faz a exclusao um aluno.")
	public void delete(@PathVariable final int idAluno) {
		alunoService.delete(idAluno);
	}

	@GetMapping("/alunos/nome/{nome}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "retorna todos Alunos passando parametro de nome com paginacao.")
	public ResponseEntity<Page<Aluno>> findAll(@PathVariable final String nome, @PageableDefault(page = 0, size = 5, sort = "id", direction = Sort.Direction.ASC)Pageable pageable) {
		return ResponseEntity.ok(alunoService.findByNome(nome, pageable));
	}

	@GetMapping("/alunos/turma/{idTurma}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Retorna todos alunos passando parametro da turma.")
	public List<Aluno> findByTurma(@PathVariable final int idTurma) {
		return alunoService.findByTurma(idTurma);
	}
	
}
