package com.demonstracao.demonstracao.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Comparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.demonstracao.demonstracao.domain.entity.Turma;
import com.demonstracao.demonstracao.service.TurmaService;

@RestController
@RequestMapping(path = "/api", produces = { MediaType.APPLICATION_JSON_VALUE })
@CrossOrigin(origins = "*")
public class TurmaController extends AbstractController {

	@Autowired
	private TurmaService turmaService;

	@CrossOrigin(origins = { "http://localhost:8100", "http://localhost:4200" })
	@GetMapping("/all")
	@ResponseStatus(HttpStatus.OK)
	public List<Turma> findAll() {
		
		
		List<Turma> dd =  turmaService.findAll().stream().sorted(Comparator.comparing(Turma::getNome)
                .thenComparing(Turma::getNome)).collect(Collectors.toList());
		
		return turmaService.findAll();
	}

	@GetMapping("/turmas")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Page<Turma>> findAll(
			@PageableDefault(page = 0, size = 5, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
		return ResponseEntity.ok(turmaService.findAll(pageable));
	}

}
