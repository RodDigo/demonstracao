package com.demonstracao.demonstracao.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.demonstracao.demonstracao.domain.entity.Navigation;
import com.demonstracao.demonstracao.service.NavigationService;

@RestController
@RequestMapping(path = "/navigation", produces = { MediaType.APPLICATION_JSON_VALUE })
public class NavigationController {

	@Autowired
	private  NavigationService navigationService; 
	
	@CrossOrigin(origins = { "http://localhost:8100", "http://localhost:4200", "http://localhost:3000" })
	@GetMapping("/all")
	@ResponseStatus(HttpStatus.OK)
	private List<Navigation> gatAll() {
		return navigationService.getAll();
	}
	
}
