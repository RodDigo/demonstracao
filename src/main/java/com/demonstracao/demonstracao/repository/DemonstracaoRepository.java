package com.demonstracao.demonstracao.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

//public class DemonstracaoRepository extends JpaRepository<Veiculo, Integer> {
@Repository
public interface DemonstracaoRepository {

	List<String> nomes();
}
