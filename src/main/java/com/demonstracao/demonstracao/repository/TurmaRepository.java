package com.demonstracao.demonstracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demonstracao.demonstracao.domain.entity.Turma;

@Repository
public interface TurmaRepository extends JpaRepository<Turma, Integer> {

}
