package com.demonstracao.demonstracao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demonstracao.demonstracao.domain.entity.Navigation;

public interface NavigationRepository extends JpaRepository<Navigation, Integer> {

	
	
	
}
