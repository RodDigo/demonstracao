package com.demonstracao.demonstracao.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demonstracao.demonstracao.domain.entity.Aluno;
import com.demonstracao.demonstracao.domain.entity.Turma;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, Integer> {

	Page<Aluno> findByNome(String nome, Pageable page);

	List<Aluno> findByNome(String nome);
	
	List<Aluno> findByTurma(Turma turma);
//	List<Aluno> findAll();

}
